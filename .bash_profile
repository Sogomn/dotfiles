export PATH="$PATH:$HOME/Programs/bin" #Custom stuff
export PATH="$PATH:$HOME/.local/bin" #Local stuff
export PATH="$PATH:$HOME/.cargo/bin" #Cargo
export PATH="$PATH:$HOME/.npm-packages/bin" #NPM
export PATH="$PATH:$HOME/.local/share/gem/ruby/3.0.0/bin" #Ruby
export PATH="$PATH:/usr/local/cuda/bin" #CUDA

export EDITOR=nvim
export VISUAL=nvim
export TERMINAL=alacritty
export BROWSER=firefox

[ -f $HOME/.bashrc ] && source $HOME/.bashrc
