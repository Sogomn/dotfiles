set nocompatible

call plug#begin('~/.local/share/nvim/plugged')

Plug 'tomasr/molokai'
Plug 'scrooloose/nerdtree'

call plug#end()

syntax on
filetype indent plugin on

" GUI
set number
set ruler
set cmdheight=1
set history=500
set scrolloff=10
set cursorline
set statusline+=%F
set termguicolors
colorscheme molokai

" Backup
set nobackup
set nowb
set noswapfile

" Indentation
set noexpandtab
set copyindent
set preserveindent
set softtabstop=0
set shiftwidth=4
set tabstop=4
set nowrap

" Misc
set novisualbell
set noerrorbells
set autoread
set ignorecase
set showmatch
