# i3 config file (v4)

# Variables
set $mod Mod4
set $ws1 1
set $ws2 2
set $ws3 3
set $ws4 4
set $ws5 5
set $ws6 6
set $ws7 7
set $wsbig 10
set $darkest #272822
set $dark #555555
set $bright #999999
set $brightest #cccccc
set $transparent_dark #000000aa
set $white #F8F8F0
set $bar_height 35
set $window_padding 15
set $font_family Source Sans 3 Semibold
set $font_size 13
set $floating_size 1250 750
set $monitor1 DP-4
set $monitor2 DP-2

# Font
font pango:$font_family $font_size

# Workspace assignments
exec --no-startup-id i3-msg "workspace $ws1"
assign [class="(?i)Code"] $ws2
assign [class="(?i)Firefox"] $ws3
assign [class="(?i)Thunderbird"] $ws4
assign [class="(?i)Gimp"] $ws5
assign [class="(?i)Discord"] $ws6
assign [class="(?i)Spotify"] $ws7
assign [class="(?i)Rofi"] output $monitor1

# Terminal
bindsym $mod+Return exec --no-startup-id alacritty

# Launcher
bindsym $mod+d exec --no-startup-id rofi -show run

# Explorer
bindsym $mod+e exec --no-startup-id thunar
for_window [class="(?i)Thunar" window_type="normal"] floating enable, resize set $floating_size
for_window [class="(?i)Xarchiver" window_type="normal"] floating enable, resize set $floating_size

# Change splitting direction
bindsym $mod+a split toggle

# Fullscreen
bindsym $mod+f fullscreen toggle

# Floating
floating_modifier $mod
bindsym $mod+space floating toggle

# Sticky
bindsym $mod+m sticky toggle

# Close window
bindsym $mod+q kill

# Focus
bindsym $mod+s focus mode_toggle
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right
focus_wrapping no

# Move windows
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# Assign workspaces to monitors
workspace $ws1 output $monitor1
workspace $ws2 output $monitor1
workspace $ws3 output $monitor1
workspace $ws4 output $monitor1
workspace $ws5 output $monitor1
workspace $ws6 output $monitor2
workspace $ws7 output $monitor1
workspace $wsbig output $monitor2

# Switch workspace
bindsym $mod+1 workspace $ws1, focus tiling
bindsym $mod+2 workspace $ws2, focus tiling
bindsym $mod+3 workspace $ws3, focus tiling
bindsym $mod+4 workspace $ws4, focus tiling
bindsym $mod+5 workspace $ws5, focus tiling
bindsym $mod+6 workspace $ws6, focus tiling
bindsym $mod+7 workspace $ws7, focus tiling
bindsym $mod+0 workspace $wsbig, focus tiling
bindsym --whole-window $mod+button4 workspace prev_on_output, focus tiling
bindsym --whole-window $mod+button5 workspace next_on_output, focus tiling

# Move windows between workspaces
bindsym $mod+Shift+1 move to workspace $ws1
bindsym $mod+Shift+2 move to workspace $ws2
bindsym $mod+Shift+3 move to workspace $ws3
bindsym $mod+Shift+4 move to workspace $ws4
bindsym $mod+Shift+5 move to workspace $ws5
bindsym $mod+Shift+6 move to workspace $ws6
bindsym $mod+Shift+7 move to workspace $ws7
bindsym $mod+Shift+0 move to workspace $wsbig, workspace $wsbig, focus tiling

# Restart, exit
bindsym $mod+Shift+r restart
bindsym $mod+Shift+e exit

# Top bar
bar {
	id 0
	output $monitor1
	i3bar_command i3bar -t
	status_command i3blocks
	position top
	mode dock
	font pango:$font_family $font_size
	separator_symbol ""
	height $bar_height
	tray_output none
	binding_mode_indicator no
	strip_workspace_numbers yes
	colors {
		background $transparent_dark
		focused_workspace $transparent_dark $transparent_dark $white
		active_workspace $transparent_dark $transparent_dark $white
		inactive_workspace $transparent_dark $transparent_dark $bright
		urgent_workspace $transparent_dark $transparent_dark $bright
		separator $brightest
		statusline $white
	}
}

# Bottom bar
bar {
	id 1
	output $monitor1
	status_command i3blocks -c ~/.config/i3blocks/config_empty #Temporary hack while i3bar is being weird
	position bottom
	mode hide
	hidden_state hide
	height $bar_height
	tray_output $monitor1
	tray_padding 5
	binding_mode_indicator yes
	workspace_buttons no
	modifier none
	colors {
		background $darkest
		binding_mode $darkest $dark $brightest
	}
}
bindsym $mod+t bar hidden_state toggle 1

# Right monitior bar
bar {
	id 2
	output $monitor2
	i3bar_command i3bar -t
	status_command i3blocks -c ~/.config/i3blocks/config_empty #Temporary hack while i3bar is being weird
	position top
	mode dock
	font pango:$font_family $font_size
	separator_symbol ""
	height $bar_height
	tray_output none
	binding_mode_indicator no
	strip_workspace_numbers yes
	colors {
		background $transparent_dark
		focused_workspace $transparent_dark $transparent_dark $white
		active_workspace $transparent_dark $transparent_dark $white
		inactive_workspace $transparent_dark $transparent_dark $bright
		urgent_workspace $transparent_dark $transparent_dark $bright
		separator $brightest
		statusline $white
	}
}

# Colors (Border, Background, Text, Indicator, Child border)
client.focused $brightest $darkest $brightest $darkest $brightest
client.focused_inactive $dark $darkest $brightest $darkest $bright
client.unfocused $dark $darkest $brightest $darkest $dark
client.urgent $dark $darkest $brightest $darkest $dark
client.background $darkest

# Borders
for_window [all] border pixel 1
default_border pixel 1
default_floating_border pixel 1
hide_edge_borders none
smart_borders on

# Gaps
gaps inner $window_padding
gaps outer 0
bindsym $mod+plus gaps outer current plus 10
bindsym $mod+minus gaps outer current minus 10

# Screenshots
bindsym Print exec --no-startup-id ~/Scripts/screenshot_select.sh
bindsym Shift+Print exec --no-startup-id ~/Scripts/screenshot.sh

# Wallpaper
exec --no-startup-id ~/.fehbg

# Brightness
#bindsym XF86MonBrightnessUp exec --no-startup-id ~/Scripts/brightness_up.sh
#bindsym XF86MonBrightnessDown exec --no-startup-id ~/Scripts/brightness_down.sh

# Audio
bindsym XF86AudioRaiseVolume exec --no-startup-id ~/Scripts/volume_up.sh
bindsym XF86AudioLowerVolume exec --no-startup-id ~/Scripts/volume_down.sh
bindsym XF86AudioMute exec --no-startup-id ~/Scripts/volume_toggle.sh

# Spotify
bindsym XF86AudioNext exec --no-startup-id playerctl next
bindsym XF86AudioPrev exec --no-startup-id playerctl previous
bindsym XF86AudioPlay exec --no-startup-id playerctl play-pause

# Blue light filter
#exec --no-startup-id redshift -l 53.5511:9.9937

# Spotify song launcher
bindsym $mod+g exec --no-startup-id ~/Scripts/spotify_menu.sh

# Welcome
exec --no-startup-id aplay -N ~/Music/sounds/start.wav

# Thunar daemon
exec --no-startup-id thunar --daemon
