#!/bin/sh

SINK=$(pactl info | grep "Default Sink" | awk '{print $3}')

pactl set-sink-volume $SINK -5% && pkill -SIGRTMIN+1 i3blocks
