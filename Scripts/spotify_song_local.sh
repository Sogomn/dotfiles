#!/bin/sh

SONG=$(playerctl -p spotify metadata -f "{{artist}} - {{title}}")

if [ $? = 0 ]
then
	echo $SONG
else
	echo ""
fi
