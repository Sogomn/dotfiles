#!/bin/sh

SONG=$(rofi -dmenu -p  -config ~/.config/rofi/config_single_line.rasi)

if [ "$?" != "0" ]
then
	exit 1
fi

URI=$(~/Scripts/spotify_lookup.sh "$SONG")

if [ -n "$URI" ]
then
	playerctl open "$URI"
fi
