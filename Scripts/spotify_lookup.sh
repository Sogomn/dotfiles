#!/bin/sh

if [ -z "$1" ]
then
	exit 1
fi
AUTH_KEY=$(bat ~/Documents/SPOTIFY_AUTH | xargs)
RESPONSE=$(curl -X "POST" --silent -H "Authorization: Basic $AUTH_KEY" --data-urlencode "grant_type=client_credentials" "https://accounts.spotify.com/api/token")
TOKEN=$(echo "$RESPONSE" | jq -r '.access_token')
RESPONSE=$(curl -G --silent -H "Authorization: Bearer $TOKEN" --data-urlencode "q=$1" --data-urlencode "type=track" --data-urlencode "limit=1" "https://api.spotify.com/v1/search")
SONG=$(echo "$RESPONSE" | jq -r '.tracks.items[0].uri')

if [ "$SONG" = "null" ]
then
	exit 1
fi

echo $SONG
