#!/bin/sh

WEATHER=$(curl --silent -N "wttr.in/Hamburg,Germany?0Q")

if [ ! -z "$WEATHER" ]
then
	echo "$WEATHER"
fi
