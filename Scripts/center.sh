#!/bin/sh

while read LINE
do
	LENGTH=$(echo $LINE | wc -m)
	WIDTH=$(tput cols)
	START=$(expr $WIDTH / 2 - $LENGTH / 2)

	if [ $START -ge "0" ]
	then
		printf %*s%s $START "" "$LINE"
		echo
	fi
done
