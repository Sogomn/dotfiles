#!/bin/sh

SINK=$(pactl info | grep "Default Sink" | awk '{print $3}')

pactl set-sink-mute $SINK toggle && pkill -SIGRTMIN+1 i3blocks
