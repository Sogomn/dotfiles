#!/bin/sh

ID=$(~/Scripts/current_window_id.sh)
MODE=$(xprop -id $ID | grep -i "I3_FLOATING_WINDOW" | awk '{print $3}')

if [ "$MODE" = "1" ]
then
	MODE="Floating"
else
	MODE="Tiling"
fi

echo $MODE
