#!/bin/sh

ID=$(~/Scripts/current_window_id.sh)
NAME=$(xprop -id $ID | grep _NET_WM_NAME | awk '{print $NF}' | tr -d '"')

echo $NAME
