# Only execute this file in an interactive shell
[[ $- != *i* ]] && return

# Terminal prompt
PS1="\W \$ "

# Enable colors
alias ls="ls --color"

# Better padding
alias neofetch="echo; neofetch | sed \"s/^/  /\""

# Just an alias
alias code="run code-oss"

# Weather script
alias weather="sh ~/Scripts/weather.sh"

# Alacritty is not recognized by ssh (yet)
alias ssh="TERM=xterm-256color ssh"

# Start calendar week at Monday, not Sunday
alias cal="cal -m"

# Start new terminal instance at current directory
alias cloneterm="run alacritty --working-directory ."

# Enable color code interpretation
alias less="less -R"
