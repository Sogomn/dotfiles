#!/bin/sh

# Always execute from containing directory
SCRIPT_PATH=$(dirname "$0")
cd $SCRIPT_PATH

# Cleanup
rm -r .config
rm -r Scripts
rm -r etc-X11-xorg.conf.d

# Create directories
mkdir -p ".config"
mkdir -p ".config/Thunar"
mkdir -p ".config/Code - OSS/User"
mkdir -p "etc-X11-xorg.conf.d"

# Copy files
cp ~/".config/Thunar/uca.xml" ".config/Thunar/"
cp ~/".bashrc" "."
cp ~/".bash_profile" "."
cp ~/".Xmodmap" "."
cp ~/".xinitrc" "."
cp ~/".Xresources" "."
cp ~/".config/picom.conf" ".config/"
cp ~/".config/Code - OSS/User/settings.json" ".config/Code - OSS/User/"
cp ~/".config/Code - OSS/User/keybindings.json" ".config/Code - OSS/User/"
cp "/etc/X11/xorg.conf.d/"* "etc-X11-xorg.conf.d/"

# Copy directories
cp -r ~/".config/feh" ".config/"
cp -r ~/".config/dunst" ".config/"
cp -r ~/".config/i3" ".config/"
cp -r ~/".config/i3blocks" ".config/"
cp -r ~/".config/alacritty" ".config/"
cp -r ~/".config/rofi" ".config/"
cp -r ~/".config/neofetch" ".config/"
cp -r ~/".config/nvim" ".config/"
cp -r ~/".config/fontconfig" ".config/"
cp -r ~/".config/bat" ".config/"
cp -r ~/"Scripts" .
