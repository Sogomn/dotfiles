# Void Linux / i3-gaps dotfiles

### Void packages:
* NetworkManager
* Thunar
* alacritty
* alsa-firmware
* alsa-lib
* alsa-plugins
* alsa-plugins-pulseaudio
* alsa-tools
* alsa-utils
* apache-maven-bin
* aspell-en
* audacity
* base-devel
* base-system
* bat
* cloc
* cmake
* cmatrix
* cronie
* curl
* dunst
* elogind
* feh
* ffmpeg
* firefox
* font-adobe-source-code-pro
* font-awesome
* font-inconsolata-otf
* gimp
* git
* gnome-font-viewer
* gradle
* grub-customizer
* grub
* gscreenshot
* gtk-engine-murrine
* gtk2-engines
* gvfs
* hashcat
* i3-gaps
* i3blocks
* inkscape
* intel-ucode
* jq
* liberation-fonts-ttf
* libnotify
* light
* linux-firmware
* linux-firmware-network
* linux-headers
* lxappearance
* lxsession
* lxtask
* neofetch
* neovim
* net-tools
* network-manager-applet
* nodejs
* noto-fonts-emoji
* nss-mdns
* ntp
* nvidia
* openjdk
* openvpn
* p7zip
* papirus-icon-theme
* pavucontrol
* picom
* playerctl
* pulseaudio
* python3-devel
* python3-pip
* redshift
* rofi
* rsync
* ruby-devel
* sl
* source-sans-pro
* texlive-bin
* thunar-archive-plugin
* thunar-volman
* thunderbird
* tree
* tumbler
* vlc
* void-repo-multilib
* void-repo-multilib-nonfree
* void-repo-nonfree
* void-updates
* vscode
* xarchiver
* xbindkeys
* xclip
* xdg-utils
* xdotool
* xorg
* xsel
* xtools
* youtube-dl

---

### Additional setup
* Set `flat-volumes = no` in `/etc/pulse/daemon.conf` - Enables independent volume control
* `amixer -c [CARD] sset "Auto-Mute Mode" Disabled` - Disable auto-mute for sound card `[CARD]`
* `mkdir "$HOME/.npm-packages" && npm config set prefix "$HOME/.npm-packages"` - Install NPM packages locally so it doesn't require root anymore

---

### Restricted packages (`void-packages` repo)
* spotify
* discord

### Ruby gems:
* neovim (`gem install --user neovim`)

### Node packages
* neovim (`npm install -g neovim`)

### Pip3 packages
* neovim (`pip3 install --user neovim`)

### Manual installation
* [run](https://gitlab.com/Sogomn/run)
* [rustup](https://rustup.rs/)
* [vim-plug](https://github.com/junegunn/vim-plug)

---

### Theme:
* [Vimix](https://github.com/vinceliuice/vimix-gtk-themes)

### Icons:
* Papirus

### Mouse:
* [BMZ](https://www.gnome-look.org/p/1158321/)

---

![Screenshot](scrot1.jpg)
![Screenshot](scrot2.jpg)
![Screenshot](scrot3.jpg)

---

### File structure
This the following directories are assumed in `~/`:
```
.
+ --- Documents/
|     + --- todo.txt
+ --- Images/
|     + --- wallpapers/
|           + --- ...
|     + --- screens/
+ --- Scripts/
|     + --- blocks/
|           + --- ...
|     + --- ...
| --- Music/
|     + --- sounds/
|           + --- start.wav
```

---

### Services
The following services are enabled in `/var/service/`:
* alsa
* avahi-daemon
* cronie
* dbus
* dhcpcd
* elogind
* polkitd
* pulseaudio
* sshd
* udevd
* uuidd

---

### Navigation
Default modifier is the windows key

|Key|Function|
|---|---|
|Mod + Enter|Open terminal|
|Mod + D|Open application launcher|
|Mod + \[Number\]|Switch to workspace|
|Mod + Mouse wheel scoll|Switch workspace|
|Mod + Shift + \[Number\]|Move window to workspace|
|Mod + Arrow key|Switch focus|
|Mod + Shift + Arrow key|Move window|
|Mod + Q|Close window|
|Mod + Space|Put window in floating mode|
|Mod + F|Put window in fullscreen mode|
|Mod + S|Toggle focus between tiling and floating|
|Mod + A|Switch between horizontal and vertical tiling direction|
|Mod + M|Put window in sticky mode|
|Mod + Shift + R|Reload i3|
|Mod + Shift + E|Exit i3|
|Mod + T|Open tray bar|
|Mod + E|Open Thunar|
|Mod + G|Open Spotify song launcher|
|Mod + Mouse drag|Move floating window|
|Mod + Right mouse drag|Resize window|
|Mod + Plus|Increase inner gaps|
|Mod + Minus|Decrease inner gaps|
|Print|Take a screenshot by selecting a rectangle with the mouse|
|Shift + Print|Save a screenshot to `~/Images/screenshot.png`|

##### Sticky mode
The window will always be on your current workspace when in floating mode
